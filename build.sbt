lazy val root = (project in file("."))
  .settings(commonSettings)

lazy val commonSettings = Seq(
  version := "0.1",
  scalaVersion := "2.13.4",
  libraryDependencies ++= commonDeps,
  resolvers += Resolver.mavenLocal,
  resolvers += "Artifactory" at "https://macrohealth.jfrog.io/artifactory/mh-maven-dev-local/",
  credentials += Credentials(Path(".sbt/.credentials").asFile)
)

val circeVersion = "0.13.0"
val circeOpticsVersion = "0.13.0"

val commonDeps = Seq(
  "com.github.blemale" %% "scaffeine" % "5.1.0",
  "com.chatwork" %% "scala-jwk" % "1.2.24",
  "io.circe" %% "circe-literal" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-optics" % circeOpticsVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.circe" %% "circe-generic-extras" % circeVersion,
  "org.log4s" %% "log4s" % "1.8.2",
  "com.macrohealth.platform" %% "x12-validating-parser" % "2.7.1-ci.4",
  "org.scalatest" %% "scalatest" %  "3.2.2",
  "org.apache.logging.log4j" % "log4j-core" % "2.16.0"
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}