# Parse and Diff 999 Tool

## Purpose
Compare 999's files from FLAMBAE vs PLATFORM for the same claims.


## Setup
Since this is a Scala-based application, installation of Scala and Java JVM is required.


## Usage
To run via IDE:
1. Run the Main class (src/main/scala/com/macrohealth/diff/utils/Main.scala)
2. Enter the folder name containing all the Flambae 999 files. Make sure there are no files besides 999s inside the folder such as .DSStore
3. Enter the folder name containing all the Platform 999 files. Make sure there are no files besides 999s inside the folder such as .DSStore
4. Enter the text file path to save the results.



```shell
Enter FLAMBAE 999 folder directory (ex./Users/fannywong/flambae) :  /Users/fannywong/Desktop/flambae999
Enter PLATFORM 999 folder directory (ex./Users/fannywong/platform): /Users/fannywong/Desktop/platform999
Enter file path for results text output (ex. (ex./Users/fannywong/999.txt): /Users/fannywong/Desktop/999.txt
```
