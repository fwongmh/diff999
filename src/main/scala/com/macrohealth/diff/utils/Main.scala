package com.macrohealth.diff.utils

import scala.io.StdIn.readLine

object Main{

  // Main method
  def main(args: Array[String])
  {
    print("Enter FLAMBAE 999 folder directory (ex./Users/fannywong/flambae) :  ")
    val flambaeDir = readLine()
    print("Enter PLATFORM 999 folder directory (ex./Users/fannywong/platform): ")
    val platformDir = readLine()
    print("Enter file path for results text output (ex. (ex./Users/fannywong/999.txt): ")
    val outputFile = readLine()
    ParseAndDiff.parseAndOutputResults(flambaeDir, platformDir, outputFile)
  }
}
