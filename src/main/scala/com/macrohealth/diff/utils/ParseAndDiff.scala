package com.macrohealth.diff.utils

import com.macrohealth.diff.utils.ParseX12FIleUtils.loadSchemaAndParseMultipleFiles
import com.macrohealth.platform.vix12.structure.{X12Loop, X12Segment}
import com.macrohealth.platform.vix12.validator.CodeValidator
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.io.{File, PrintWriter}
import scala.io.Source
import scala.util.control.Breaks.{break, breakable}

object ParseAndDiff extends AnyFlatSpec with Matchers {

  def getListOfFiles(dir: String): List[String] = {
    val file = new File(dir)
    file.listFiles
      .map(_.getPath).toList
  }

  def parseAndOutputResults(flam999SourceDirectory: String, platform999SourceDirectory: String, outputFilePath: String): Unit = {
    val pw = new PrintWriter(new File(outputFilePath))
    val inSchema = Source.fromResource("schemas/Macro_005010_999.json")
    val inDetails = Source.fromResource("schemas/Macro_00501.json")
    val inCustomization = Source.fromString("[]")
    val flambae999List = getListOfFiles(flam999SourceDirectory)
    val platform999List = getListOfFiles(platform999SourceDirectory)

    val parsedResults = loadSchemaAndParseMultipleFiles(
      structureSchema = inSchema,
      segmentsSchema = inDetails,
      customizations = inCustomization,
      platX12FileList = platform999List,
      flamX12FileList = flambae999List,
      send999AcceptsProperty = "true",
      splitSegment = "",
      enrich = false,
      config = Map(),
      codeValidator = CodeValidator(Map()),
      conditionalRules = None
    )

    val platParsed = parsedResults._1
    val flamParsed = parsedResults._2

    for (f <- platParsed) {
      breakable {
        var fileNameToBeProcessed = f.fileName.get
        val prefix = "PLAT_"
        val suffix = "_999.x12"
        fileNameToBeProcessed = fileNameToBeProcessed.substring(prefix.length, fileNameToBeProcessed.length)
        fileNameToBeProcessed = fileNameToBeProcessed.substring(0, fileNameToBeProcessed.length - suffix.length)
        pw.println()
        pw.println(s"File: ${fileNameToBeProcessed}")

        val platformSegments = f.transactionSets.head.segments
        val flambaeTransactionSet = flamParsed.filter(x => x.fileName.get.contains(fileNameToBeProcessed))

        if (flambaeTransactionSet.size == 0) {
          pw.println()
          pw.println("-------------------ERROR--------------------")
          pw.println(s"No corresponding file found in Flambae folder: ${fileNameToBeProcessed}")
          pw.println(s"Skipping : ${fileNameToBeProcessed}")
          pw.println()
          break;
        }

        val flambaeSegments = flambaeTransactionSet.head.transactionSets.head.segments
        val flambae2000Loop = flambaeSegments.filter(x => x.schemaLoc.name == "2000")
        val platform2000Loop = platformSegments.filter(x => x.schemaLoc.name == "2000")

        var platFoundErrors = 0
        var flamFoundErrors = 0
        var bothFoundErrorsCount = 0

        pw.println(s"AK202, SEGMENT, PLATFORM_IK501, FLAMBAE_IK501")
        for (x <- platform2000Loop) {
          val platform = x.segmentOrLoop.asInstanceOf[X12Loop]
          val ak2 = platform.dataElements.filter(x => x.schemaLoc.name == "AK2")
          val ak202 = ak2.head.segmentOrLoop.asInstanceOf[X12Segment].elements.apply(1).leftSideValue.value.left.getOrElse()

          val flambaeSearch = flambae2000Loop.filter(x => x.segmentOrLoop.asInstanceOf[X12Loop].dataElements.apply(0).segmentOrLoop.asInstanceOf[X12Segment].elements.apply(1).leftSideValue.value.left.getOrElse().equals(ak202))
          val flambae = flambaeSearch.head.segmentOrLoop.asInstanceOf[X12Loop]

          if (flambaeSearch.size == 1) {
            val pIk5 = platform.dataElements.filter(x => x.schemaLoc.name.equals("IK5")).apply(0).segmentOrLoop.asInstanceOf[X12Segment]
            val fIk5 = flambae.dataElements.filter(x => x.schemaLoc.name.equals("IK5")).apply(0).segmentOrLoop.asInstanceOf[X12Segment]

            val pIk5Value = pIk5.elements.apply(0).leftSideValue.value.left.getOrElse()
            val fIk5Value = fIk5.elements.apply(0).leftSideValue.value.left.getOrElse()

            // Both PLAT and FLAM IK501 value is same
            if (pIk5Value.equals(fIk5Value)) {
              // IK501 value is not A
              if (!pIk5Value.equals("A")) {
                bothFoundErrorsCount += 1
              }
            }
            else if (!pIk5Value.equals("A")) {
              platFoundErrors += 1
            }
            else if (!fIk5Value.equals("A")) {
              flamFoundErrors += 1
            }

            if (!pIk5.equals(fIk5)) {
              pw.println(s"${ak202}, IK5, ${pIk5Value}, ${fIk5Value}")
            }
          }
          else {
            pw.println(s"AK2 No Match: ${ak202}")
          }
        }
        pw.println(s"UNIQUE PLAT FOUND ERRORS: ${platFoundErrors}")
        pw.println(s"UNIQUE FLAM FOUND ERRORS: ${flamFoundErrors}")
        pw.println(s"BOTH FOUND ERRORS: ${bothFoundErrorsCount}")
        pw.println()
      }
    }
    pw.close()
  }
}
