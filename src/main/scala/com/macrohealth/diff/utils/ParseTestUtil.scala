package com.macrohealth.diff.utils

import com.macrohealth.platform.vix12.ParseNotifier
import com.macrohealth.platform.vix12.parser.Vix12Parser
import com.macrohealth.platform.vix12.plugins.conditionalrule.{ConditionalPlugin, ConditionalRule}
import com.macrohealth.platform.vix12.schema.{QualifiersSegmentSchema, SchemaLoader}
import com.macrohealth.platform.vix12.structure.Vix12Configurations.{SEND_999_ACCEPTS, SPLIT_SEGMENT_LOOP}
import com.macrohealth.platform.vix12.structure.Vix12StructureExtractor.TA104Result
import com.macrohealth.platform.vix12.structure._
import com.macrohealth.platform.vix12.validator.ValidationErrorCodes.InterchangeNoteCode
import com.macrohealth.platform.vix12.validator.plugin.ValidatorPlugin
import com.macrohealth.platform.vix12.validator.{CodeValidator, X12Schema, X12ValidatingParser}
import io.circe.Json
import io.circe.parser.parse
import io.circe.syntax.EncoderOps
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

import java.io.File
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

object ParseX12FIleUtils {
  val passingCodeValidator: CodeValidator = CodeValidator(Map())

  case class ParseResults(qualifiers: Option[QualifiersSegmentSchema],
                          validationResults: InterchangeControlValidationResults,
                          transactionSets: mutable.ListBuffer[ParsedTransactionSet],
                          segmentList: List[X12Segment],
                          rawWarnings: Json = Json.Null,
                          fileName: Option[String] = None
                         )

  def loadSchemaAndParseMultipleFiles(structureSchema: Source,
                                      segmentsSchema: Source,
                                      customizations: Source,
                                      platX12FileList: List[String],
                                      flamX12FileList: List[String],
                                      send999AcceptsProperty: String,
                                      splitSegment: String,
                                      enrich: Boolean,
                                      config: Map[String, String] = Map(),
                                      codeValidator: CodeValidator = passingCodeValidator,
                                      conditionalRules: Option[List[ConditionalRule]] = None): (ListBuffer[ParseResults], ListBuffer[ParseResults]) = {


    val processedResults = new ListBuffer[ListBuffer[ParseResults]]()
    val schema: X12Schema = loadSchema(structureSchema, segmentsSchema, customizations)
    val qualifiers = Option.when(enrich)(schema.segmentSchema)
    val vix12Config = Vix12Configurations(config + (SEND_999_ACCEPTS -> send999AcceptsProperty) + (SPLIT_SEGMENT_LOOP -> splitSegment))
    for (x <- List(platX12FileList, flamX12FileList)) {
      val parsedResultsList = new ListBuffer[ParseResults]()
      for (y <- x) {
        var as = List[X12Segment]()

        val transactionSets = ListBuffer[ParsedTransactionSet]()
        val transactionSetValidations = mutable.ListBuffer[TransactionSetValidationResults]()

        val plugins: Set[ValidatorPlugin] = updateConditionalRulePlugin(
          config = vix12Config,
          existingPlugins = X12ValidatingParser.plugins(vix12Config, codeValidator),
          customRules = conditionalRules
        )
        val x12ValidatingParser = new X12ValidatingParser(schema, codeValidator, vix12Config, plugins)
        val se = Vix12StructureExtractor(x12ValidatingParser, new ParseNotifier {
          def isaHeaderProcessed(isaHeader: ISAHeader, warnings: List[InterchangeNoteCode]): Unit = {}

          override def additionalSegments(header: ISAHeader, segments: List[X12Segment]): Unit = {
            as = segments
          }

          override def gsHeaderProcessed(header: ISAHeader, gsHeader: GSHeader): Unit = {}

          def transactionProcessed(isaHeader: ISAHeader, gsHeader: GSHeader, transactionSet: ParsedTransactionSet, transactionSetValidationResults: TransactionSetValidationResults): Unit = {
            if (splitSegment == "") {
              transactionSets += transactionSet
              transactionSetValidations += transactionSetValidationResults
            }
          }

          override def splitSegmentLoop(isaHeader: ISAHeader, gsHeader: GSHeader, stHeader: STHeader, transactionSet: ParsedTransactionSet, transactionSetValidationResults: TransactionSetValidationResults): Unit = {
            if (splitSegment != "") {
              transactionSets += transactionSet
              transactionSetValidations += transactionSetValidationResults
            }
          }
        }, vix12Config)

        val parser = new Vix12Parser(Source.fromFile(y))
        val parsed = se.parseX12(parser)
        val parsedResult = ParseResults(
          qualifiers = qualifiers,
          validationResults = parsed,
          transactionSets = transactionSets,
          segmentList = as,
          rawWarnings = if (transactionSetValidations.nonEmpty) transactionSetValidations.head.rawWarnings() else Json.Null,
          fileName = Some(new File(y).getName)
        )
        parsedResultsList += parsedResult
      }
      processedResults += parsedResultsList
    }

    val platParseResult = processedResults.apply(0)
    val flamParseResult = processedResults.apply(1)
    (platParseResult, flamParseResult)
  }


  private def updateConditionalRulePlugin(config: Vix12Configurations,
                                          existingPlugins: Set[ValidatorPlugin],
                                          customRules: Option[List[ConditionalRule]]): Set[ValidatorPlugin] = {
    existingPlugins.foldLeft(Set.empty[ValidatorPlugin])((acc, plugin) => {
      if (plugin.isInstanceOf[ConditionalPlugin]) {
        acc + new ConditionalPlugin(config = config, conditionalRules = customRules)
      }
      else acc + plugin
    })
  }

  private def validate(parsed: InterchangeControlValidationResults,
                       qualifiers: Option[QualifiersSegmentSchema],
                       validationsDateGS: String,
                       validationsDateISA: String,
                       validationsTime: String,
                       enrich: Boolean = false,
                       expectedValidations: Source
                      ) = {
    var validationsDateReplaced = parsed.encode(qualifiers).hcursor.downField("meta").downField("ISA09").downField("value").withFocus(_.mapString(_ => validationsDateISA)).top
    validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("meta").downField("ISA10").downField("value").withFocus(_.mapString(_ => validationsTime)).top
    validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("segments").withFocus(_.mapArray(_.map(_.hcursor.downField("TA102").downField("value").withFocus(_.mapString(_ => validationsDateISA)).top.get))).top
    validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("segments").withFocus(_.mapArray(_.map(_.hcursor.downField("TA103").downField("value").withFocus(_.mapString(_ => validationsTime)).top.get))).top
    if (enrich) {
      validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("meta").downField("ISA09").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderDate(validationsDateISA))).top
      validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("meta").downField("ISA10").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderTime(validationsTime))).top
      validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("segments").withFocus(_.mapArray(_.map(_.hcursor.downField("TA102").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderDate(validationsDateISA))).top.get))).top
      validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("segments").withFocus(_.mapArray(_.map(_.hcursor.downField("TA103").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderTime(validationsTime))).top.get))).top
    }
    if (parsed.functionalGroupValidationResults.nonEmpty && !parsed.ack.ackCode.equals(TA104Result.Rejected)) {
      validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("functionalgroups").withFocus(_.mapArray(_.map(_.hcursor.downField("meta").downField("GS04").downField("value").withFocus(_.mapString(_ => validationsDateGS)).top.get))).top
      validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("functionalgroups").withFocus(_.mapArray(_.map(_.hcursor.downField("meta").downField("GS05").downField("value").withFocus(_.mapString(_ => validationsTime)).top.get))).top
      if (enrich) {
        validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("functionalgroups").withFocus(_.mapArray(_.map(_.hcursor.downField("meta").downField("GS04").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderDate(validationsDateGS))).top.get))).top
        validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("functionalgroups").withFocus(_.mapArray(_.map(_.hcursor.downField("meta").downField("GS05").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderTime(validationsTime))).top.get))).top
        validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("segments").withFocus(_.mapArray(_.map(_.hcursor.downField("TA102").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderDate(validationsDateISA))).top.get))).top
        validationsDateReplaced = validationsDateReplaced.get.asJson.hcursor.downField("segments").withFocus(_.mapArray(_.map(_.hcursor.downField("TA103").downField("displayValue").withFocus(_.mapString(_ => prettyHeaderTime(validationsTime))).top.get))).top
      }
    }
    validationsDateReplaced.get shouldBe parse(expectedValidations.mkString).getOrElse("Failed to parse expected validation results")
  }

  def loadSchema(structureSchema: Source, segmentsSchema: Source, customizations: Source): X12Schema = {
    val schema = SchemaLoader.loadSchema(structureSchema, segmentsSchema, customizations.mkString) match {
      case Right(value) => value
      case Left(e) => throw new IllegalArgumentException(e.message)
    }
    schema
  }

  private def prettyHeaderDate(dateStr: String): String = {
    dateStr.length match {
      case 6 => s"${dateStr.subSequence(0, 2)}-${dateStr.subSequence(2, 4)}-${dateStr.subSequence(4, 6)}"
      case 8 => s"${dateStr.subSequence(0, 4)}-${dateStr.subSequence(4, 6)}-${dateStr.subSequence(6, 8)}"
      case _ => dateStr
    }
  }

  private def prettyHeaderTime(timeStr: String): String = {
    timeStr.length match {
      case 4 => s"${timeStr.subSequence(0, 2)}:${timeStr.subSequence(2, 4)}"
      case 6 => s"${timeStr.subSequence(0, 2)}:${timeStr.subSequence(2, 4)}:${timeStr.subSequence(4, 6)}"
      case 8 => s"${timeStr.subSequence(0, 2)}:${timeStr.subSequence(2, 4)}:${timeStr.subSequence(4, 6)}.${timeStr.subSequence(6, 8)}"
      case _ => timeStr
    }
  }

  def parseToJson(str: String): Json = {
    parse(str).getOrElse(org.scalatest.Assertions.fail(s"not a valid json: $str"))
  }
}
